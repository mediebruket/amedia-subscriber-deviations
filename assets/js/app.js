$(document).ready(function(){
  initValidate();
  initDatepicker();
  initToggleTitles();
});

function initDatepicker(){

  $('.datepicker').datepicker({
      format: 'dd.mm.yyyy',
      language: 'no',
      weekStart: 1,
      beforeShowDay: function(date){
          var today =  new Date();
          if ( date >= today)
            return false;
          else
            return;
        }
      // datesDisabled: disabled_days,
      // daysOfWeekDisabled: days_of_week,
    });


    if($('.datepickerFrom').length && $('.datepickerTo').length){
      $('.datepickerFrom').change( function(){
        var fromDatoUtc = $('.datepickerFrom').datepicker('getUTCDate');
        var toDatoUtc = $('.datepickerTo').datepicker('getUTCDate');

        if ( fromDatoUtc > toDatoUtc){
          $('.datepickerTo').datepicker('setDate', $('.datepickerFrom').val() );
        }
      });

      $('.datepickerTo').change( function(){
        var fromDatoUtc = $('.datepickerFrom').datepicker('getUTCDate');
        var toDatoUtc = $('.datepickerTo').datepicker('getUTCDate');

        if ( fromDatoUtc == null || fromDatoUtc > toDatoUtc){
          $('.datepickerFrom').datepicker('setDate', $('.datepickerTo').val() );
        }
      });
    }

}

function initValidate(){
  $(".js-validation").validate();
}

function initToggleTitles(){
  $('#toggle-options').change(function(){
    if ( $('#toggle-options').prop('checked') ){
      $('.title-code').prop('checked', true);
    }
    else{
     $('.title-code').prop('checked', false);
    }
  });
}