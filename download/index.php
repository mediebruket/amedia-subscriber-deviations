<?php
session_start();
include('../config/conf.php');
include('../lib/util.php');
include('../lib/Csv.php');

if ( isset($_SESSION['results']) ){
	$attr = getConfig('attributes.php' );
	$Csv = new Csv($_SESSION['results'], $attr);
	$Csv->writeFile();

	header('Content-Description: File Transfer');
  header('Content-Type: text/csv');
  header('Content-Disposition: attachment; filename='.basename($Csv->HttpFilepath));
  header('Content-Transfer-Encoding: binary');
  header('Expires: 0');
  header('Cache-Control: must-revalidate');
  header('Pragma: public');
  header('Content-Length: ' . filesize($Csv->Filepath));

  readfile($Csv->HttpFilepath);
}
?>