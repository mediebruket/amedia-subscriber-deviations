<?php
session_start();

$users = array('mediebruket' => 'admin', 'amedia' => 'deviations');
$is_auth = false;

$username = null;
$password = null;

function requireAuth(){
	header('WWW-Authenticate: Basic realm="My Realm"');
  header('HTTP/1.0 401 Unauthorized');
  echo 'Text to send if user hits Cancel button';
  die();
}

// mod_php
if (isset($_SERVER['PHP_AUTH_USER'])) {
    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];

// most other servers
} elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {

  if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0)
    list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));

}

if (is_null($username)) {
	requireAuth();
}
else {
	if ( isset($users[$username]) && $users[$username] == $password ){
		$is_auth = true;
	}
}

if(!$is_auth):
	requireAuth();
else: ?>
<?php ini_set('max_execution_time', 10000); ?>
<?php include('config/conf.php'); ?>
<?php include('lib/util.php'); ?>
<?php include('lib/SoapUtil.php'); ?>
<?php include('lib/WSSoapClient.php'); ?>
<?php include('lib/SubscriberDeviations.php'); ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 ielt7" lang="nn-NO"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7 ielt8" lang="nn-NO"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8 ielt9" lang="nn-NO"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="nn-NO"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="nb-NO"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/app.css?t=<?php echo time(); ?>" />
		<link rel="stylesheet" type="text/css" href="assets/css/datepicker.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<script src="assets/js/bootstrap.datepicker.js" ></script>
		<script src="assets/js/bootstrap.datepicker.no.js" ></script>
		<script src="assets/js/app.js?t=<?php echo time(); ?>" ></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
						<?php
						// get title codes
						$title_codes = getConfig('title-codes.php');
						?>

						<?php // request  ?>
						<?php $result = null; ?>
						<?php if ( isset($_POST['posted']) ): ?>
						<?php
							$args = $_POST;
							$products = array();
							// prepare parameter products
							if ( isset($args['product']) && is_array($args['product']) ){
								foreach ($args['product'] as $index => $title){
									if ( isset($title_codes[$title]) ){
										$products[] = array('_' => $title, 'customerSystem' => $title_codes[$title]['cr']);
									}
								}

								$args['product'] = $products;

								$Sd = new SubscriberDeviations();
								$result = $Sd->soap_GetSubscriberDeviationsByProducts($args);
							}
							else{
								$result = 'Ingen avis valgt';
							}
						?>
						<?php endif;  ?>

						<?php // soap error ?>
						<?php if ( is_string($result) ): ?>
							<div class="alert alert-danger"><?php echo $result; ?></div>
						<?php endif; ?>

						<!-- form -->
						<form action="" method="POST" class="js-validation di-form ">
								<input type="hidden" name="posted" value="1" />
							  <div class="form-group">
							  	<label for="fromDate">Fra</label>
							  	<input type="text" name="fromDate" class="datepicker datepickerFrom" id="fromDate" value="<?php echo( (isset($_POST['fromDate'])) ? $_POST['fromDate'] : null ); ?>" required />
							  </div>
							  <div class="form-group">
							  	<label for="toDate">Til</label>
							  	<input type="text" name="toDate" class="datepicker datepickerTo" id="toDate" value="<?php echo( (isset($_POST['toDate'])) ? $_POST['toDate'] : null ); ?>" required />
							  </div>
							  <?php if( is_array($title_codes) ): ?>
							  <div class="form-group">
							  	<label for="toggle-options" class="toggle-options" ><input type="checkbox" id="toggle-options" value="" > Alle</label>
							  	<div class="title-codes">
							  	<?php foreach ( $title_codes as $code => $Title): ?>
							  		<div class="title">
							  			<label for="<?php echo $code; ?>" >
							  				<input class="title-code" type="checkbox" id="<?php echo $code; ?>" name="product[]" value="<?php echo $code; ?>" <?php echo ( (isset($_POST['product']) && is_numeric(array_search($code, $_POST['product'])) ) ? ' checked="checked" ' : '' ); ?>>
							  				<?php echo $Title['name']; ?>
							  			</label>
							  		</div>
							  		<?php endforeach; ?>
							  	</div>
							  </div>
								<?php endif; ?>
								<button type="submit" class="btn btn-primary">Send</button>
						</form>

						<!-- result -->
							<?php  if ( isset($result->deviation) && is_array($result->deviation) && count($result->deviation) ): ?>
							<div class="export"><div class=""><i class="fa fa-download"></i> <a href="download/">CSV</a></div></div>
								<?php $visible_attr = getConfig('attributes.php'); ?>
								<?php $_SESSION['results'] = $result->deviation; ?>
									<!-- result table -->
									<table class="table table-condensed">
										<!-- table head -->
										<thead>
											<?php foreach ($visible_attr as $key => $value): ?>
												<td><?php echo preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $value); ?></td>
											<?php endforeach; ?>
										</thead>
										<!-- table body -->
										<tbody>
										<?php foreach ($result->deviation as $key => $Deviation): ?>
											<tr>
												<?php foreach ($visible_attr as $i => $value): ?>
													<td><?php echo ( (isset($Deviation->$value)) ? $Deviation->$value : null ); ?></td>
												<?php endforeach; ?>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
							<?php endif; ?>

						</div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php endif; ?>