<?php

class Csv{
	public $Csv;
	public $Filepath;
	public $HttpFilepath;
	private $Attr;
	private $Results;
	private $CsvHead = array();
	private $CsvBody = array();

	function __construct($results, $attr){
		if ( is_array($attr) ){
			$this->Attr = $attr;
			$this->prepareHeader();

			if ( is_array($results) ){
				$this->Results = $results;
				$this->prepareBody();
			}
		}
	}


	function writeFile(){

		$filename = 'deviations'.time().'.csv';
		$file_dir = 'files/';

		$this->Filepath = APP_PATH.$file_dir.$filename;
		$this->HttpFilepath = APP_URL. $file_dir.$filename;
		$fp = fopen($this->Filepath, 'w');

		$csv = $this->CsvHead.$this->CsvBody;
		fwrite($fp, $csv);
		fclose($fp);
	}

	function prepareHeader(){
		$head = array();
		foreach ($this->Attr as $key => $value){
			$head[] = preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $value);
		}

		$this->CsvHead = $this->implode($head);
	}



	function prepareBody(){
		$body = null;

		foreach ($this->Results as $key => $Deviation){
			$row = array();

			foreach ($this->Attr as $i => $value){
		 		$row[] = (isset($Deviation->$value)) ? $Deviation->$value : '' ;
		 	}

		 	$body .= $this->implode($row);
		}

		$this->CsvBody = $body;
	}

	function implode($array){
		return implode(';', $array)."\n";
	}

}

?>