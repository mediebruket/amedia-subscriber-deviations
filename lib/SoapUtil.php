<?php
	class SoapUtil{

		function __construct(){
		}

		function dateToSoapDate( $datepicker_date ){
			$date = explode(".", $datepicker_date );
			return $date[2]."-".$date[1]."-".$date[0];
		}

		function debug($message = null){
			print_r('<pre>');
			var_dump($message);
			print_r('</pre>');
		}

	}
?>