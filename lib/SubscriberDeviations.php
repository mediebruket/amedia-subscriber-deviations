<?php

class SubscriberDeviations extends SoapUtil{
	public $client;

	function __construct(){
		$wsdl = 'http://www.di.no/doc/webservices/ws/wsdl/GetSubscriberDeviationsByProductsV1.wsdl';
		// $wsdl = 'http://www.di.no/doc/webservices/ws/wsdl/FindDeviationsBySubscriberV1.wsdl';
		$login = 'wsamedia';
		$password = "40Ey9q788Q7D67b";

		$options =
			array(
			'soap_version'=> SOAP_1_1,
			'trace'				=> true,
			'login'				=> $login,
			'password'		=> $password,
		);

		try{
			$this->client = new WSSoapClient( $wsdl, $options );
			$this->client->__setUsernameToken($login, $password, 'PasswordText');

		}
		catch (Exception $e){
			$this->debug("Class SubscriberDeviations: webservice not available");
		}
	}


	function soap_GetSubscriberDeviationsByProducts( $args=null ){
		$message = null;
		try{

			// set fromDate
			$args['fromDate'] = $this->dateToSoapDate($args['fromDate']);

			// set toDate
			$args['toDate'] = $this->dateToSoapDate($args['toDate']);

			// set product
			// $product_title = $args['product'];
			// $args['product'] = array('_'  => $product_title, 'customerSystem' => $args['customerSystem'] );



			$result = $this->client->GetSubscriberDeviationsByProducts( $args );

			// $this->debug('soap_GetSubscriberDeviationsByProducts');
			// $this->debug($args);
			// $this->debug($result);

			return $result;
		}

		catch (Exception $e){
			// $this->debug($this->client->__getLastRequestHeaders() );
			// $this->debug('-----');
			//$this->debug($this->client->__getLastRequest());
			$message = $e->getMessage();
		}

		if ( isset($result->deviation) ){
			return $result->deviation;
		}
		else{
			return $message;
		}
	}
}

?>